from django.urls import path

from . import views

urlpatterns = [
    path('<str:poem>', views.poem_view, name='poem_view')
]
