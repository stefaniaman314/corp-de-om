from django.http import HttpResponse, Http404

# Create your views here.
from django.shortcuts import render


def poem_view(request, poem):
    """
    Return the appropriate poem view based on poem id.

        * buna_seara: sGpYxfSF2A
        * dragoste_de_caisa: 76W0K45Dld
        * eu_pe_mine_ma: Iti8gjLEwW
        * gleznele_mele: CESBQc2jg4
        * iubire_sau_dor: Y-zC6W27Nz
        * la_scaldat: KkI-Kk1O8z
        * la_zugravit: jfR3gbEVne
        * ma_fut: F535kLBhwN
        * natura_noastra_de_nestiutori: M8YyqBw1fs
        * ghici_ciuperca: epj7ldCVAN
        * planor: nm3RlbBprn
        * scurte: eRSyTQTEP5
    """
    if poem == "sGpYxfSF2A":
        return render(request, 'poems/poem1.html')
    elif poem == "76W0K45Dld":
        return render(request, 'poems/poem2.html')
    elif poem == "Iti8gjLEwW":
        return render(request, 'poems/poem3.html')
    elif poem == "CESBQc2jg4":
        return render(request, 'poems/poem4.html')
    elif poem == "Y-zC6W27Nz":
        return render(request, 'poems/poem5.html')
    elif poem == "KkI-Kk1O8z":
        return render(request, 'poems/poem6.html')
    elif poem == "jfR3gbEVne":
        return render(request, 'poems/poem7.html')
    elif poem == "F535kLBhwN":
        return render(request, 'poems/poem8.html')
    elif poem == "M8YyqBw1fs":
        return render(request, 'poems/poem9.html')
    elif poem == "epj7ldCVAN":
        return render(request, 'poems/poem10.html')
    elif poem == "nm3RlbBprn":
        return render(request, 'poems/poem11.html')
    elif poem == "eRSyTQTEP5":
        return render(request, 'poems/poem12.html')
    else:
        raise Http404("Poem does not exist.")



